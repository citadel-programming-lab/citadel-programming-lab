"""
Citadel Programming Lab
Copyright (C) 2017-present  Heriot-Watt University, Glasgow School of Art

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import sys, re, glob, os, time, logging, platform, random, uuid, base64, hashlib, string
import gitlab
from requests import post
from flask import Flask, request, render_template, url_for, redirect, session
from flask_wtf import FlaskForm
from wtforms import StringField, RadioField, SubmitField, Field, SelectMultipleField, IntegerField, TextAreaField, widgets, BooleanField
from wtforms.validators import DataRequired
from flask_bootstrap import Bootstrap
from collections import Counter

app = Flask(__name__)
app.secret_key = 'CHANGE_ME'
bootstrap = Bootstrap(app)

APP_PATH = "/usr/src/app/"
DATA_PATH = "/usr/data/"
while not os.path.exists(os.path.join(DATA_PATH, "host")):
    # the machine has just been created and is not ready yet
    time.sleep(5)
with open(os.path.join(DATA_PATH, "host"), "r") as hostfile:
    HOSTNAME = hostfile.read().strip()
GAMESERVER_INTERNAL_IP = "https://172.18.0.4:5000"
GITLAB_INTERNAL_IP = "https://"+HOSTNAME
GAMESERVER_PUBLIC_IP = "https://"+HOSTNAME+":5000"
GITLAB_PUBLIC_IP = "https://"+HOSTNAME
CONFIG_PATH = os.path.join(DATA_PATH, ".python-gitlab.cfg")
logging.basicConfig(filename=os.path.join(DATA_PATH, "app-combined.log"), level=logging.DEBUG)


@app.after_request
def allow_gitlab_through_security(response):
    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Origin
    if request.origin in [GAMESERVER_PUBLIC_IP, GITLAB_PUBLIC_IP, GITLAB_INTERNAL_IP]:
        response.headers["Access-Control-Allow-Origin"] = request.origin
        response.headers["Access-Control-Allow-Headers"] = "Origin, Content-Type, Accept, Token, X-Gitlab-Token"
        response.headers["Access-Control-Allow-Methods"] = "GET, OPTIONS, POST"
    return response

# Helper methods

def validate_user():
    if request.json and 'token' in request.json:
        usertoken = request.json['token']
    elif 'Token' in request.headers:
        usertoken = request.headers['Token']
    elif request.form and 'token' in request.form:
        usertoken = request.form['token']
    elif request.args and 'token' in request.args:
        usertoken = request.args['token']
    elif 'token' in session:
        usertoken = session['token']
    else:
        raise Exception("Could not auth user, no token. Try relaunching the game.")
    try:
        gl = gitlab.Gitlab(GITLAB_INTERNAL_IP, oauth_token=usertoken, ssl_verify="/gitlab-chain.pem")
        gl.auth()
        if not gl.user:
            raise Exception("Failed to verify token - user not populated. Try relaunching the game.")
        return gl.user
    except Exception:
        logging.info("Failed to contact gitlab - expired token?. Try relaunching the game.")

# teacher logins are verified by requiring them to provide an API token for login
# its not perfect, but it s better than nothing
def admin_session():
    if 'token' not in session:
        return False
    gl = gitlab.Gitlab(GITLAB_INTERNAL_IP, oauth_token=session['token'], ssl_verify="/gitlab-chain.pem")
    gl.auth()
    return gl.user.is_admin

list_of_tasks = [
        "credentials-task",
        "url-task",
        "encryption-task",
        "ssl-task",
        "pgp-task",
        "x509-task"
    ]

def validate_task(user):
    taskname = ""
    if request.json and 'task' in request.json:
        taskname = request.json['task']
    elif 'task' in request.headers:
        taskname = request.headers['task']
    elif request.form and 'task' in request.form:
        taskname = request.form['task']
    elif request.args and 'task' in request.args:
        taskname = request.args['task']
    else:
        raise Exception("no task name available")
    if taskname not in list_of_tasks:
        raise Exception("Task not accepted")
    gl = gitlab.Gitlab.from_config('gameserver', [CONFIG_PATH])
    fulluser = gl.users.get(user.id)
    taskstub = fulluser.projects.list(search=taskname)[0]
    return gl.projects.get(taskstub.id)

def is_setup():
    return os.path.exists(CONFIG_PATH)

# LEADERBOARD

def leaderboard_path(user):
    return os.path.join(DATA_PATH, "leaderboards", user)

@app.route("/get")
def leaderboard_get():
    leaderboard = []
    for user in glob.glob(leaderboard_path("*")):
        with open(user, 'r') as file:
            data = file.read().split(",")
            leaderboard.append({"username":"".join(data[1:]), "score":int(data[0])})
    return {"scores":leaderboard}

@app.route("/update", methods=['POST'])
def leaderboard_update():
    try:
        user = validate_user()
    except Exception as e:
        return str(e), 401
    newscore = int(request.json['newscore'])
    if newscore < 0 or newscore > sys.maxsize:
        newscore = 0
    filename = leaderboard_path(user.username)
    try:
        with open(filename, "r") as file:
            oldscore = int(file.read())
    except:
        oldscore = 0
    if newscore > oldscore:
        os.makedirs(os.path.dirname(filename), exist_ok=True)
        with open(filename, "w") as file:
            file.write(str(newscore)+","+str(user.name))
    return leaderboard_get()

# ADMIN

@app.route("/", methods=["GET", "POST"])
def index():
    if request.method == "POST" and 'token' in request.form:
        # we are logging in
        gl = gitlab.Gitlab(GITLAB_INTERNAL_IP, oauth_token=request.form['token'], ssl_verify="/gitlab-chain.pem")
        gl.auth()
        if not gl.user.is_admin:
            return "The token provided was not an admin token", 403
        session['token'] = request.form['token']
    if is_setup() and admin_session():
        # site is set up, we are logged in
        return redirect("/teacher")
    if is_setup() and not admin_session():
        # site is set up, we are not logged in
        return render_template("setupform.html", hostname=HOSTNAME)
    if not is_setup() :
        # site is not set up, we meed to do first time setup
        return redirect("/setup")

def task_state(gl, user, taskname):
    try:
        proj = user.projects.list(search=taskname)[0]
    except:
        # this user did not have the correct projects forked
        return "noinit"
    project = gl.projects.get(proj.id)
    latestpipes = project.pipelines.list()
    if len(latestpipes) == 0:
        return "started" if is_task_started(user, taskname) else "unstarted"
    latestjobs = latestpipes[0].jobs.list()
    latestpy = latestjobs[0] if latestjobs[0].name == "python" else latestjobs[1]
    latestjava = latestjobs[0] if latestjobs[0].name == "java" else latestjobs[1]
    success = latestjava.status=="success" or latestpy.status=="success"
    return success

def stream_template(template_name, **context):
    # https://flask.palletsprojects.com/en/2.0.x/patterns/streaming/
    app.update_template_context(context)
    t = app.jinja_env.get_template(template_name)
    rv = t.stream(context)
    rv.enable_buffering(5) #default 5
    return rv

def metric_reporting_on():
    return os.path.exists(os.path.join(DATA_PATH, "metrics_enabled"))

@app.route("/teacher/metrics/toggle", methods=["GET", "POST"])
def teacher_metric_toggle():
    if not admin_session():
        return str("Not logged in as admin"), 401
    path = os.path.join(DATA_PATH, "metrics_enabled")
    form = AdminConsentForm()
    if request.method == "GET":
        return render_template("adminconsent.html", form=form)
    else:
        if form.validate_on_submit():
            os.makedirs(os.path.dirname(path), exist_ok=True)
            report_to_mainserver("SITE_CONSENT", "root", form.consent.data, consent_override=True)
            if form.consent.data:
                with open(path, "w") as fp:
                    fp.write("1")
                return "Metric reporting enabled, pursuant to student consent"
            else:
                if os.path.exists(path):
                    os.remove(path)
                return "Metric reporting disabled globally"
        else:
            return render_template('adminconsent.html', form=form)

def lang_for_user(username, task=None, format="text"):
    if not isinstance(username, str):
        username = username.username
    if task is not None and not isinstance(task, str):
        task = task.path
    j = 0
    p = 0
    path = os.path.join(DATA_PATH, "metrics", "server", "SOL_START")
    if os.path.exists(path):
        with open(path, "r") as mfile:
            for line in mfile:
                if task is None:
                    if re.match(username+",\\d+\\.\\d+,p", line):
                        p += 1
                    elif re.match(username+",\\d+\\.\\d+,j", line):
                        j += 1
                else:
                    if re.match(username+",\\d+\\.\\d+,python,"+task, line):
                        p += 1
                    elif re.match(username+",\\d+\\.\\d+,java,"+task, line):
                        j += 1
    return "j:"+str(j)+" p:"+str(p) if format == "text" else (j,p)


@app.route("/teacher")
def teacher_view():
    if not admin_session():
        return str("Not logged in as admin"), 401

    all_users = []
    gl = gitlab.Gitlab.from_config('gameserver', [CONFIG_PATH])
    usernames = gl.users.list()

    playspath = os.path.join(DATA_PATH, "metrics", "game", "GL_ENDED")
    countofgames = 0
    if os.path.exists(playspath):
        with open(playspath, "r") as playmetrics:
            for line in playmetrics:
                countofgames += 1
    gamedata = {
        'avg': countofgames / len(usernames)
    }
    metricmessage = "Metric reporting is "
    metricmessage = metricmessage + "enabled" if metric_reporting_on() else metricmessage + "disabled"
    metricmessage = metricmessage + ". Click to toggle"
    def get_student_summaries():
        for user in usernames:
            path_received = reviews_recieved(user.username, "*")
            path_given = reviews_given(user.username, "*")
            globbed_recv = glob.glob(path_received)
            globbed_give = glob.glob(path_given)
            scorepath = leaderboard_path(user.username)
            if os.path.exists(scorepath):
                with open(scorepath, 'r') as leaderboard:
                    score = leaderboard.read()
            else:
                score = "--"
            userdata = {
                'uid': user.username,
                'task1': task_state(gl, user, list_of_tasks[0]),
                'task2': task_state(gl, user, list_of_tasks[1]),
                'task3': task_state(gl, user, list_of_tasks[2]),
                'task4': task_state(gl, user, list_of_tasks[3]),
                'task5': task_state(gl, user, list_of_tasks[4]),
                'task6': task_state(gl, user, list_of_tasks[5]),
                'lang': lang_for_user(user.username),
                'reviewsrec': len(globbed_recv),
                'reviewsgiv': len(globbed_give),
                'hiscore': score
            }
            all_users.append(userdata)
            yield userdata
    warnings = []
    if os.path.exists(os.path.join(DATA_PATH, "warnings")):
        with open(os.path.join(DATA_PATH, "warnings"), "r") as warnfile:
            for warn in warnfile:
                warnings.append(warn)
    warntext = ""
    if len(warnings) > 0:
        warntext = "<h1>Warnings</h1><ol>"+"\n<li>".join(warnings)+"</ol>"
    with open(os.path.join(DATA_PATH, "install_guid"), "r") as uuidfile:
        UUID = uuidfile.read().strip()
    return app.response_class(stream_template('teacher_admin.html', all_users=get_student_summaries(), gamedata=gamedata, warnings=warntext, metricmessage=metricmessage, uuid=UUID))

@app.route("/teacher/student/<student>")
def student_detail(student):
    if not admin_session():
        return str("Not logged in as admin"), 401

    gl = gitlab.Gitlab.from_config('gameserver', [CONFIG_PATH])
    users = gl.users.list(username=student)
    if len(users) == 0:
        return "User not found", 404
    user = users[0]
    scorepath = leaderboard_path(user.username)
    if os.path.exists(scorepath):
        with open(scorepath, 'r') as leaderboard:
            score = leaderboard.read()
    else:
        score = "--"
    playspath = os.path.join(DATA_PATH, "metrics", "game", "GL_ENDED")
    countofgames = 0
    if os.path.exists(playspath):
        with open(playspath, "r") as playmetrics:
            for line in playmetrics:
                if student in line:
                    countofgames += 1
    game = {
        'hiscore': score,
        'count': countofgames
    }
    upgpath = os.path.join(DATA_PATH, "metrics", "game", "G_UPG_UN")
    upgs = ""
    if os.path.exists(upgpath):
        with open(upgpath, "r") as upgfile:
            for line in upgfile:
                if student in line:
                    upgs = upgs + "," + line.split(",")[2]
    tasks = []
    for taskname in list_of_tasks:
        path_received = reviews_recieved(user.username, taskname)
        path_given = reviews_given(user.username, taskname)
        path_finished = task_is_complete_by_name(user.username, taskname)
        globbed_recv = glob.glob(path_received)
        globbed_give = glob.glob(path_given)
        tasks.append({
            'id': taskname,
            'state': task_state(gl, user, taskname),
            'lang': lang_for_user(user, taskname),
            'reviewsrec': len(globbed_recv),
            'reviewsgiv': len(globbed_give),
            'commits': str(len(gl.projects.get(user.projects.list(search=taskname)[0].id).commits.list())),
            'finished': os.path.exists(path_finished),
            'unlocked': upgs
        })
    reviews = []
    path_received = reviews_recieved(user.username, "*")
    path_given = reviews_given(user.username, "*")
    globbed_recv = glob.glob(path_received)
    globbed_give = glob.glob(path_given)
    for review in globbed_recv:
        path = review.split(os.path.sep)
        with open(review, "r") as rfile:
            reviews.append({
                'from': path[-1],
                'to': path[-3],
                'task': path[-2],
                'content': rfile.read()
            })
    for review in globbed_give:
        path = review.split(os.path.sep)
        with open(review, "r") as rfile:
            reviews.append({
                'from': path[-3],
                'to': path[-1],
                'task': path[-2],
                'content': rfile.read()
            })
    return render_template("teacher_single_student.html", reviews=reviews, user=user, tasks=tasks, game=game, hostname=HOSTNAME)

@app.route("/setup", methods=["GET", "POST"])
def first_run_setup():
    logging.info("Did initial setup of token")
    if not is_setup() and request.method == "GET":
        # we are not set up, no one is logged in
        # we need the first visitor to complete an admin login
        return render_template("setupform.html", hostname=HOSTNAME)
    if is_setup() and request.method == "GET" and admin_session():
        # we are want to reset the token
        return render_template("setupform.html", hostname=HOSTNAME)
    if (not is_setup() and request.method == "POST" and 'token' in request.form) or \
       (is_setup() and admin_session() and request.method == "POST" and 'token' in request.form):
        # we are running setup for the first time
        # or we are resetting the access token
        gl = gitlab.Gitlab(GITLAB_INTERNAL_IP, oauth_token=request.form['token'], ssl_verify="/gitlab-chain.pem")
        gl.auth()
        if not gl.user.is_admin:
            return "The token provided was not an admin token", 403
        session['token'] = request.form['token']
        with open(CONFIG_PATH, "w") as configfile:
            configfile.write(f"""[global]
ssl_verify = /gitlab-chain.pem
timeout = 5
api_version = 4

[gameserver]
url = {GITLAB_INTERNAL_IP}
private_token = {request.form['token']}
ssl_verify = /gitlab-chain.pem
""")
        userlist = [] if request.form['userstoadd'].strip() == "" else [user.split(",") for user in request.form['userstoadd'].strip().split("\n")]
        return app.response_class(makeGitlabProjects(userlist, request.form['token']))
    # we have had an unauthorised access
    return "You can only run setup via the form during initial setup or later once already logged in as an admin", 403

def fork_a_project(user, project):
    gl = gitlab.Gitlab.from_config('gameserver', [CONFIG_PATH])
    projects = user.projects.list(search=project.path)
    if len(projects) == 0:
        time.sleep(2)
        new_fork = project.forks.create({'namespace': user.username})
        fork_status = gl.projects.get(new_fork.id, lazy=True).imports.get()
        while fork_status.import_status != 'finished':
            time.sleep(2)
            fork_status.refresh()
        created_project = gl.projects.get(new_fork.id)
        created_project.delete_fork_relation()
        time.sleep(2)
        created_project.hooks.create({
            'url': GAMESERVER_INTERNAL_IP+'/hook',
            'push_events': 1, # for commits
            'pipeline_events': 1, # for CI status
            'token': gen_hook_token(user.username, created_project.path),
            'enable_ssl_verification': False
        })
        logging.debug("Forked project %s for user %s" % (project.path, user.username))
    else:
        logging.debug("Already forked project %s for user %s" % (project.path, user.username))

def makeGitlabProjects(userlist, token):
    # add the initial projects
    yield "<html><head><title>Setting up NetSecLab</title></head><body><p>Setting Up..."
    try:
        if not os.path.exists(os.path.join(DATA_PATH, "install_uuid")):
            with open(os.path.join(DATA_PATH, "install_guid"), "w") as uuidfile:
                uuidfile.write(str(uuid.uuid4()))

        logging.info("Setting up initial projects, users and forks")
        gl = gitlab.Gitlab.from_config('gameserver', [CONFIG_PATH])
        user = gl.users.list(username = 'root')[0]
        yield "<p>Added root user"

        # BRANDING
        glapp = gl.appearance.get()
        glapp.title = "Citadel Programming Lab"
        glapp.description = "The Citadel Programming Lab, a secure coding game-based lab originally developed jointly by Heriot-Watt University and the Glasgow School of Art with support from RISCS/NCSC, the EPSRC Secrious Project, and CyBOK."
        glapp.save()
        os.system(""" curl --location --request PUT "%s/api/v4/application/appearance?data=image/png"\
 --header "Content-Type: multipart/form-data"\
 --header "PRIVATE-TOKEN: %s"\
 --form "logo=@%s/static/citadel-programming-lab-logo.png" """ % (GITLAB_PUBLIC_IP, token, APP_PATH))

        os.system("""curl --location --request PUT "%s/api/v4/application/appearance?data=image/png"\
 --header "Content-Type: multipart/form-data"\
 --header "PRIVATE-TOKEN: %s"\
 --form "favicon=@%s/static/favicon.ico" """ % (GITLAB_PUBLIC_IP, token, APP_PATH))

        os.system("""curl --location --request PUT "%s/api/v4/application/appearance?data=image/png"\
 --header "Content-Type: multipart/form-data"\
 --header "PRIVATE-TOKEN: %s"\
 --form "header_logo=@%s/static/header.png" """ % (GITLAB_PUBLIC_IP, token, APP_PATH))


        def import_a_project(user, file, name):
            # NOTE TO SELF: when you get something from list() it often is missing many attributes. you need to get() by id for all of them
            tasks = user.projects.list(search=name)
            if len(tasks) == 0:
                newtask = gl.projects.import_project(
                    open(os.path.join(APP_PATH, 'lab', file), 'rb'),
                    path=name,
                    namespace=user.username
                )
                project_import = gl.projects.get(newtask['id'], lazy=True).imports.get()
                while project_import.import_status != 'finished':
                    time.sleep(1)
                    project_import.refresh()
                logging.debug("imported "+name)
                return gl.projects.get(newtask['id'])
            else:
                logging.debug("already imported "+name)
                return gl.projects.get(tasks[0].id)

        t1 = import_a_project(user, 'cred-task.gz', 'credentials-task')
        time.sleep(1)
        t2 = import_a_project(user, 'enc-task.gz', 'encryption-task')
        time.sleep(1)
        t3 = import_a_project(user, 'url-task.gz', 'url-task')
        time.sleep(4)
        t4 = import_a_project(user, 'ssl-task.gz', 'ssl-task')
        time.sleep(4)
        t5 = import_a_project(user, 'pgp-task.tar.gz', 'pgp-task')
        time.sleep(4)
        t6 = import_a_project(user, 'x509-task.gz', 'x509-task')
        yield "<p>Imported base projects"
        # add users
        logging.debug("adding users")
        userlist.append(('aa001','aa001@localhost',str(random.randint(1000000, sys.maxsize))))
        userlist.append(('bb002','bb002@localhost',str(random.randint(1000000, sys.maxsize))))
        for username, useremail, password in userlist:
            time.sleep(1)
            if len(gl.users.list(username=username)) == 0:
                gl.users.create({
                    'email':useremail,
                    'password': password,
                    'username': username,
                    'name': username,
                    'reset_password': False,
                    'skip_confirmation': True,
                    'projects_limit': 6,
                    'shared_runners_minutes_limit': 6*60,
                    'can_create_group': False
                })
            yield "<p>added user "+username

        good_pool_user = gl.users.list(username='aa001')[0]
        g1 = import_a_project(good_pool_user, 'cred-task-good.tar.gz', 'credentials-task')
        time.sleep(1)
        g2 = import_a_project(good_pool_user, 'enc-task-good.tar.gz', 'encryption-task')
        time.sleep(1)
        g3 = import_a_project(good_pool_user, 'url-task-good.tar.gz', 'url-task')
        time.sleep(1)
        g4 = import_a_project(good_pool_user, 'ssl-task-good.gz', 'ssl-task')
        time.sleep(1)
        g5 = import_a_project(good_pool_user, 'pgp-good.tar.gz', 'pgp-task')
        time.sleep(1)
        g6 = import_a_project(good_pool_user, 'x509-task-good.gz', 'x509-task')
        time.sleep(1)
        yield "<p>imported 'good' pre-completed tasks"
        bad_pool_user = gl.users.list(username='bb002')[0]
        b1 = import_a_project(bad_pool_user, 'cred-task-bad.tar.gz', 'credentials-task')
        time.sleep(1)
        b2 = import_a_project(bad_pool_user, 'enc-task-bad.tar.gz', 'encryption-task')
        time.sleep(1)
        b3 = import_a_project(bad_pool_user, 'url-task-bad.tar.gz', 'url-task')
        time.sleep(1)
        b4 = import_a_project(bad_pool_user, 'ssl-task-bad.gz', 'ssl-task')
        time.sleep(1)
        b5 = import_a_project(bad_pool_user, 'pgp-bad.tar.gz', 'pgp-task')
        time.sleep(1)
        b6 = import_a_project(bad_pool_user, 'x509-task-bad.gz', 'x509-task')
        time.sleep(1)
        yield "<p>imported 'bad' pre-completed tasks"
        do_mark_task_complete(good_pool_user, g1)
        time.sleep(1)
        do_mark_task_complete(good_pool_user, g2)
        time.sleep(1)
        do_mark_task_complete(good_pool_user, g3)
        time.sleep(1)
        do_mark_task_complete(good_pool_user, g4)
        time.sleep(1)
        do_mark_task_complete(good_pool_user, g5)
        time.sleep(1)
        do_mark_task_complete(good_pool_user, g6)
        time.sleep(1)
        do_mark_task_complete(bad_pool_user, b1)
        time.sleep(1)
        do_mark_task_complete(bad_pool_user, b2)
        time.sleep(1)
        do_mark_task_complete(bad_pool_user, b3)
        time.sleep(1)
        do_mark_task_complete(bad_pool_user, b4)
        time.sleep(1)
        do_mark_task_complete(bad_pool_user, b5)
        time.sleep(1)
        do_mark_task_complete(bad_pool_user, b6)
        time.sleep(1)
        yield "<p>finished preparing pre-completed tasks"

        secret_hook_dir_path = os.path.join(DATA_PATH, "secret_hook_tokens")
        os.makedirs(secret_hook_dir_path, exist_ok=True)
        yield "<p>prepared access token for hooks"

        # fork the tasks for each user
        for username, useremail, password in userlist:
            logging.debug("making forks for user %s" % username)
            newuser = gl.users.list(username = username)[0]
            fork_a_project(newuser, t1)
            time.sleep(1)
            fork_a_project(newuser, t2)
            time.sleep(1)
            fork_a_project(newuser, t3)
            time.sleep(1)
            fork_a_project(newuser, t4)
            time.sleep(1)
            fork_a_project(newuser, t5)
            time.sleep(1)
            fork_a_project(newuser, t6)
            yield "<p>Forked projects for user %s" % username

        yield "<h1><a href=\"/\">Setup complete! Click to return to main teacher page</a></h1></body></html>"
    except Exception as e:
        import traceback
        yield """<h1>Setup script stopped running.</h1>
        <p>If the problem was a timeout, usually reloading the page after waiting a short period will solve this
        <p>Otherwise, this is the problem that caused the failure:
        <p><pre>"""+traceback.format_exc()
        raise e

@app.route("/addauser", methods=["GET", "POST"])
def add_a_single_user():
    if not admin_session():
        return "Not an admin", 401
    if request.method == "GET":
        return "<html><head><title>Add a user</title></head><body><h1>Add a user</h1><form method=post><input type=text name=username placeholder='username'><input type=text name=email placeholder='email'><input type=password name=password placeholder='password'><input type=submit></form>"
    if request.method == "POST":
        username = request.form["username"]
        email = request.form["email"]
        password = request.form["password"]
        return app.response_class(delegated_add_a_single_user(username, email, password))

def delegated_add_a_single_user(username, email, password):
    yield "<html><head><title>Adding user</title></head><body><p>Setting Up..."
    try:
        gl = gitlab.Gitlab.from_config('gameserver', [CONFIG_PATH])
        if len(gl.users.list(username=username)) == 0:
            new_user = gl.users.create({
                'email':email,
                'password': password,
                'username': username,
                'name': username,
                'reset_password': False,
                'skip_confirmation': True,
                'projects_limit': 6,
                'shared_runners_minutes_limit': 6*60,
                'can_create_group': False
            })
        else:
            new_user = gl.users.get(gl.users.list(username=username)[0].id)
        yield "<p>added user "+username
        master = gl.users.get(gl.users.list(username="root")[0].id)
        t1 = gl.projects.get(master.projects.list(search="credentials-task")[0].id)
        t2 = gl.projects.get(master.projects.list(search="encryption-task")[0].id)
        t3 = gl.projects.get(master.projects.list(search="url-task")[0].id)
        t4 = gl.projects.get(master.projects.list(search="ssl-task")[0].id)
        t5 = gl.projects.get(master.projects.list(search="pgp-task")[0].id)
        t6 = gl.projects.get(master.projects.list(search="x509-task")[0].id)
        fork_a_project(new_user, t1)
        yield "<p>forked project 1"
        time.sleep(2)
        fork_a_project(new_user, t2)
        yield "<p>forked project 2"
        time.sleep(2)
        fork_a_project(new_user, t3)
        yield "<p>forked project 3"
        time.sleep(2)
        fork_a_project(new_user, t4)
        yield "<p>forked project 4"
        time.sleep(2)
        fork_a_project(new_user, t5)
        yield "<p>forked project 5"
        time.sleep(2)
        fork_a_project(new_user, t6)
        yield "<p>forked project 6"
        yield "<h1>Completed adding new user!"
    except Exception as e:
        import traceback
        yield """<h1>Add user script stopped running.</h1>
        <p>If the problem was a timeout, usually reloading the page after waiting a short period will solve this
        <p>Otherwise, this is the problem that caused the failure:
        <p><pre>"""+traceback.format_exc()
        raise e

def gen_hook_token(username, projectpath):
    if not isinstance(username, str):
        username = username.username
    if not isinstance(projectpath, str):
        projectpath = projectpath.path
    secret_hook_file_path = os.path.join(DATA_PATH, "secret_hook_tokens", username+"_"+projectpath)
    secret_hook_token = ''.join(random.choice(string.digits+string.ascii_letters) for i in range(11))
    h = hashlib.sha256()
    h.update(bytes(secret_hook_token, encoding="utf8"))
    hashed_token = h.hexdigest()
    with open(secret_hook_file_path, "w") as hook_file:
        hook_file.write(hashed_token)
    return secret_hook_token


def check_hook_token(username, projectpath, token):
    if not isinstance(username, str):
        username = username.username
    if not isinstance(projectpath, str):
        projectpath = projectpath.path
    secret_hook_file_path = os.path.join(DATA_PATH, "secret_hook_tokens", username+"_"+projectpath)
    h = hashlib.sha256()
    h.update(bytes(token, encoding="utf8"))
    hashed_token = h.hexdigest()
    with open(secret_hook_file_path, "r") as hook_file:
        actual_hash = hook_file.read().strip()
    return actual_hash==hashed_token


@app.route('/hook', methods=["POST"])
def hook():
    gltoken = request.headers['X-Gitlab-Token']
    data = request.json
    user = data['user_username'] if 'user_username' in data else data['user']['username']
    project_name = data['project']['name']
    if not check_hook_token(user, project_name, gltoken):
        logging.error("Incorrect token for file hook")
        return "", 401
    if data['object_kind'] == 'pipeline':
        logging.info("Handling pipeline for "+user)
        for build in data['builds']:
            manual_metric('CI_RUN', user, build['name'], build['status'])
    if data['object_kind'] == 'push':
        modifiedfiles = data['commits'][0]['modified']
        logging.info("Handling commit for "+user)
        gl = gitlab.Gitlab.from_config('gameserver', [CONFIG_PATH])
        commit = data['checkout_sha']
        project = gl.projects.get(data['project_id'])
        for modifiedfile in modifiedfiles:
            if 'python' in modifiedfile:
                manual_metric('SOL_START', user, 'python', project_name)
            if 'java' in modifiedfile:
                manual_metric('SOL_START', user, 'java', project_name)
            if 'requirements' in modifiedfile:
                manual_metric('SOL_START', user, 'python', project_name)
                f = project.files.raw(file_path='python/requirements.txt', ref=commit)
                decoded_file = str(f, encoding="utf8")
                manual_metric("CI_LIB", user, *[lib for lib in decoded_file.split("\n")])
            if 'pom.xml' in modifiedfile:
                manual_metric('SOL_START', user, 'java', project_name)
                f = project.files.raw(file_path='pom.xml', ref=commit)
                decoded_file = str(f, encoding="utf8")
                manual_metric("CI_LIB", user, *[lib.strip() for lib in decoded_file.split("\n") if ("artifactId" in lib or "version" in lib) and "<?xml" not in lib])
            if modifiedfile[-3:] == ".py" or modifiedfile[-5:] == ".java":
                f = project.files.raw(file_path=modifiedfile, ref=commit)
                decoded_file = str(f, encoding="utf8")
                # https://blog.ostermiller.org/finding-comments-in-source-code-using-regular-expressions/
                cleaned_file = re.sub("#[^\\r\\n]*", "# Cleaned Comment", decoded_file)
                cleaned_file = re.sub("//[^\\r\\n]*", "// Cleaned Comment", cleaned_file)
                cleaned_file = re.sub("/\\*(?:.|[\\n\\r])*?\\*/", "/* Cleaned Comment */", cleaned_file)
                encoded_file = base64.b64encode(cleaned_file.encode("utf8"))
                report_to_mainserver("FILE_COMMIT", user, project_name, commit, modifiedfile, encoded_file)
            if 'test' in modifiedfile.lower() or '.gitlab-ci.yml' in modifiedfile:
                with open(os.path.join(DATA_PATH, "warnings"), "a") as warnfile:
                    warnfile.write(time.asctime()+","+user+","+project_name+","+commit+","+modifiedfile+"\n")
                report_to_mainserver("ILLEGAL_COMMIT", user, project_name, commit, modifiedfile, consent_override=True)
                commitobj = project.commits.get(commit)
                commitobj.comments.create({'note': '@'+user+' Please do not change `'+modifiedfile+'` as this could interrupt the programming exercise, thank you.'})
                pipes = project.pipelines.list()
                for pipe in pipes:
                    pipe.cancel()
    return "", 204

# REVIEWS

def do_mark_task_complete(user, task):
    path = task_complete_path(user.username, task.path)
    os.makedirs(path, exist_ok=True)

@app.route("/task", methods=['POST'])
def mark_task_complete():
    # NOTE: checking that a task was finished propely and passed the CI
    # is a check that should be done by the game BEFORE
    # calling this API as this assumes you just want to mark something
    # as complete
    try:
        user = validate_user()
    except Exception as e:
        return str(e), 401
    task = validate_task(user)
    do_mark_task_complete(user, task)
    manual_metric('SOL_FIN', user, task.path)
    # The presence of a directory is sufficient for the task to be considered "complete"
    # Reviews received are then stored as files within this directory
    return "", 204

@app.route("/completedtasks")
def get_completed_tasks():
    try:
        user = validate_user()
    except Exception as e:
        return str(e), 401
    return {"complete": completed_tasks(user.username)}

@app.route("/oauth")
def oauth():
    code = request.args['code']
    with open(os.path.join(DATA_PATH, "oauth_appid")) as idfile:
        appid = idfile.read().strip()
    with open(os.path.join(DATA_PATH, "oauth_secret")) as secfile:
        secret = secfile.read().strip()
    response = post(GITLAB_INTERNAL_IP+"/oauth/token", data={
        "client_id": appid,
        "client_secret": secret,
        "code": code,
        "grant_type": "authorization_code",
        "redirect_uri": GAMESERVER_PUBLIC_IP+"/oauth",
        "code_verifier": "NQjleHNqxtupKRS3Qe4Lj7JPJLypP1THs9v6XvyyJjM"
    }, verify="/gitlab-chain.pem")
    token = response.json()["access_token"]
    session['token'] = token
    gl = gitlab.Gitlab(GITLAB_INTERNAL_IP, oauth_token=token, ssl_verify="/gitlab-chain.pem")
    gl.auth()
    authed_user = gl.users.get(gl.user.id)
    if 'consent_requested' in session:
        try:
            if authed_user.is_admin:
                return redirect("/teacher?token=" + token)
        finally:
            return redirect(GITLAB_PUBLIC_IP+"/assets/custom/game/index.html?token=" + token)
    else:
        session['consent_requested'] = '1'
        if user_did_consent(authed_user):
            try:
                if authed_user.is_admin: # this is a bool value but its only present in the true condition. why even bother?
                    return redirect("/teacher?token=" + token)
            finally:
                return redirect(GITLAB_PUBLIC_IP+"/assets/custom/game/index.html?token=" + token)
        else:
            return redirect("/consent?token=" + token)

def is_task_started(user, task):
    if not isinstance(user, str):
        user = user.username
    if not isinstance(task, str):
        task = task.path
    path = os.path.join(DATA_PATH, "metrics", "gitlab", "TASK_START")
    if os.path.exists(path):
        with open(path, "r") as metfile:
            for line in metfile:
                if line.find(user+",") == 0 and task in line:
                    return True
    return False

def task_is_complete(task):
    return task_is_complete_by_name(task.namespace['path'], task)

def task_is_complete_by_name(completer, taskname):
    return os.path.exists(task_complete_path(completer, taskname))

def completed_tasks(completer):
    path = task_complete_path(completer, "*")
    globbed = glob.glob(path)
    return [task.split(os.path.sep)[-1] for task in globbed]

def task_complete_path(completer, taskname):
    if not isinstance(completer, str):
        completer = completer.username
    if not isinstance(taskname, str):
        taskname = taskname.path
    return os.path.join(DATA_PATH, "tasks", completer, taskname)

def review_file(taskname, giver, reciever):
    if not isinstance(reciever, str):
        reciever = reciever.username
    if not isinstance(giver, str):
        giver = giver.username
    if not isinstance(taskname, str):
        taskname = taskname.path
    return os.path.join(DATA_PATH, "tasks", reciever, taskname, giver)

def review_exists(taskname, giver, reciever):
    return os.path.exists(review_file(taskname, giver, reciever))

def reviews_recieved(reciever, taskname):
    return review_file(taskname, "*", reciever)

def reviews_given(giver, taskname):
    return review_file(taskname, giver, "*")

@app.route("/givemeareview")
def get_review():
    try:
        user = validate_user()
    except Exception as e:
        return str(e), 401
    task = validate_task(user)
    if not task_is_complete(task):
        return "You haven't marked this task "+task.path+" as complete yet.<br>Please mark it as complete, then try for a review again."
    complete_solutions = task_complete_path("*", task.path)
    available_solutions = glob.glob(complete_solutions)
    users_to_be_reviewed = [complete_tasks.split(os.path.sep)[-2] for complete_tasks in available_solutions]
    count_for_users = Counter(users_to_be_reviewed)
    least_reviewed = reversed(count_for_users.most_common())
    for username, count in least_reviewed:
        if username == user.username:
            # don't review myself
            continue
        if review_exists(task.path, user, username):
            # don't review someone i already reviewed
            continue
        gl = gitlab.Gitlab.from_config('gameserver', [CONFIG_PATH])
        recieving_user = gl.users.list(username=username)[0]
        project = recieving_user.projects.list(search=task.path)[0]
        project = gl.projects.get(project.id)
        try:
            project.members.create({
                'user_id': user.id,
                'access_level': gitlab.REPORTER_ACCESS
            })
        except gitlab.exceptions.GitlabCreateError:
            # already added member - not an issue as will never happen
            # outside of debug environment
            pass
        return redirect("/questions/review/"+str(project.id))
    return "Ran out of solutions to review. Please contact the lecturer for an alternative", 200

@app.route("/reviews")
def read_reviews():
    try:
        user = validate_user()
    except Exception as e:
        return str(e), 401
    # tasks / username / taskname / username
    path_received = review_file("*", "*", user.username)
    path_given = review_file("*", user.username, "*")
    globbed_recv = glob.glob(path_received)
    globbed_give = glob.glob(path_given)
    # return arrays containing the name of the task for which a review was received
    return {
        "given": [task.split(os.path.sep)[-2] for task in globbed_give],
        "received": [task.split(os.path.sep)[-2] for task in globbed_recv]
    }

## questionnaire
likert_agree = [(a,a) for a in ['Strongly Agree', 'Agree', 'Neither agree nor disagree', 'Disagree', 'Strongly Disagree']]
yes_maybe_no = [(a,a) for a in ['Yes', 'Maybe', 'No']]
yes_no = [(a,a) for a in ['Yes', 'No']]
classes = [(a,a) for a in ['Undergraduate', 'Graduate', 'Online', 'Professional Training', 'No, but I did take a class where this topic was a major component', 'No']]
experience = [(a,a) for a in ['Paid / Professional', 'Unpaid / Hobby', 'None']]
gameplay_amounts = [(a,a) for a in ['Daily', 'Weekly', 'Monthly', 'Rarely', 'Never']]
education_types = [(a,a) for a in ['Undergraduate', 'Graduate', 'Professional Certification', 'No']]

# https://quizapp.readthedocs.io/_/downloads/en/master/pdf/ page 15
class LikertField(RadioField):
    # Note, You can't allow no selection of these, so they are mandatory
    pass

class TaskQuestionnaire(FlaskForm):
    task_difficult = LikertField('This task was difficult', choices=likert_agree)
    sol_correct = LikertField('I am confident my solution is correct', choices=likert_agree)
    sol_secure = LikertField('I am confident my solution is secure', choices=likert_agree)
    task_relevant = LikertField('I found this task relevant to the game', choices=likert_agree)
    game_correct = LikertField('The game prompted me to have a correct solution', choices=likert_agree)
    game_secure = LikertField('The game prompted me to have a secure solution', choices=likert_agree)
    no_link = LikertField('I do not see the link between this task and the game', choices=likert_agree)
    focus_game = LikertField('My focus was on the game not on this task', choices=likert_agree)
    enjoy_task = LikertField('I enjoyed this task more than the game', choices=likert_agree)
    sec_priv = LikertField('When you performed the task, were you thinking about security or privacy?', choices=yes_maybe_no)
    similar_past = LikertField('Have you written similar code or come across similar problems in the past?', choices=yes_maybe_no)
    what_secure = TextAreaField('What makes your solution either secure or insecure?')
    what_correct = TextAreaField('What makes your solution either correct or incorrect?')
    what_sec_priv = TextAreaField('If anything, were you thinking about security or privacy')
    what_similar_past = TextAreaField('If you have written similar code or come across similar problems in the past…     Tell us about it. When was it and what did you do; did you do something differently?')
    task_other_comments = TextAreaField('Anything else you want to say about this task?')
    submit = SubmitField('Submit')

class FinalQuestionnaire(FlaskForm):
    game_help = LikertField('The game helped me to complete the tasks well.', choices=likert_agree)
    linked = LikertField('I do not see the link between the tasks and the game.', choices=likert_agree)
    game_relevant = LikertField('I found the game is relevant to the tasks.', choices=likert_agree)
    game_distract = LikertField('I found the game distracted me from the tasks.', choices=likert_agree)
    game_story = LikertField('I was interested in the games story.', choices=likert_agree)
    game_hard = LikertField('I thought the game was hard.', choices=likert_agree)
    game_occupied = LikertField('I was fully occupied by the game.', choices=likert_agree)
    competence = LikertField('I felt competent.', choices=likert_agree)
    other_think = LikertField('I thought about other things.', choices=likert_agree)

    liked = TextAreaField('Are there any game elements you liked more?     If so, please give details of why you liked it, any strategies you used')
    unliked = TextAreaField('Are there any game elements you didn\'t like?     If so, please give details of why you didn\'t like it, what could make it better')
    end_other_comments = TextAreaField('Do you have any other comments regarding the game?')

    submit = SubmitField('Submit')

#class NumberField(IntegerField):
class NumberField(StringField):
    # we have to use a string field because an integer field doesn't
    # actually render a number HTML input, and makes it very easy to
    # throw out potentially valid responses. If I have time, I could
    # make a custom widget
    pass


class MultiCheckboxField(SelectMultipleField):
    # https://gist.github.com/doobeh/4668212
    widget = widgets.ListWidget(prefix_label=False)
    option_widget = widgets.CheckboxInput()

class DemographicQuestionnaire(FlaskForm):
    classes_security = MultiCheckboxField('What classes have you taken to do with Computer Security', choices=classes)
    count_security = NumberField('How many classes have you taken for Computer Security')
    time_security = NumberField('How many years ago was your last class for Computer Security')
    xp_security = MultiCheckboxField('Outside of education, what experiences do you have with Computer Security', choices=experience)
    years_security = NumberField('How many years have you been working with Computer Security')
    projects_security = NumberField('How many projects have you worked on with a focus on Computer Security')
    last_proj_security = NumberField('How many years ago was your last project involving Computer Security')

    classes_python = MultiCheckboxField('What classes have you taken to do with Python', choices=classes)
    count_python = NumberField('How many classes have you taken for Python')
    time_python = NumberField('How many years ago was your last class for Python')
    xp_python = MultiCheckboxField('Outside of education, what experiences do you have with Python', choices=experience)
    years_python = NumberField('How many years have you been working with Python')
    projects_python = NumberField('How many projects have you worked on with a focus on Python')
    last_proj_python = NumberField('How many years ago was your last project involving Python')

    classes_java = MultiCheckboxField('What classes have you taken to do with Java', choices=classes)
    count_java = NumberField('How many classes have you taken for Java')
    time_java = NumberField('How many years ago was your last class for Java')
    xp_java = MultiCheckboxField('Outside of education, what experiences do you have with Java', choices=experience)
    years_java = NumberField('How many years have you been working with Java')
    projects_java = NumberField('How many projects have you worked on with a focus on Java')
    last_proj_java = NumberField('How many years ago was your last project involving Java')

    total_programming = NumberField('How many years have you been programming (with any language)')
    gameplay = LikertField('How often do you play videogames', choices=gameplay_amounts)

    gender = StringField("What is your gender")
    age = NumberField("What is your age")
    education = LikertField('Are you currently a student in any of the following', choices=education_types)
    critical_program = LikertField("Are you currently employed in a job where programming is a critical role", choices=yes_no)
    critical_security = LikertField("Are you currently employed in a job where security is a critical role", choices=yes_no)
    job = StringField("What is your job")
    language = StringField("What is your primary spoken language")
    country = StringField("What country did you grow up in")

    submit = SubmitField('Submit')

class Review(FlaskForm):
    review_secure = LikertField("This solution is secure", choices=likert_agree)
    review_correct = LikertField("This solution is correct", choices=likert_agree)
    review_good = LikertField("This solution is good", choices=likert_agree)
    submit = SubmitField('Submit')

@app.route('/questions/review/<projectid>', methods=['GET', 'POST'])
def make_review(projectid):
    try:
        giving_user = validate_user()
    except Exception as e:
        return str(e), 401
    gl = gitlab.Gitlab.from_config('gameserver', [CONFIG_PATH])
    try:
        project = gl.projects.get(projectid)
    except:
        "Project not found", 404
    if not task_is_complete(project):
        return "This task is not complete yet, so cannot be reviewed", 401
    if not task_is_complete_by_name(giving_user.username, project.path):
        return "You have not yet completed this task, so you cannot review it yet", 401
    receiving_user = project.namespace['path']
    if review_exists(project, giving_user, receiving_user):
        return "You already reviewed this, please pick a different solution to review, so cannot be reviewed", 200
    jpath = '-/blob/master/src/main/java/' if not 'pgp' in project.path else '-/blob/main/src/main/java/'
    ppath = '-/blob/master/python/' if not 'pgp' in project.path else '-/blob/main/python/'
    which = lang_for_user(receiving_user, project.path, "pair")
    jfile = "".join([a.capitalize() for a in project.path.split("-")])+".java"
    pfile = project.path.replace("-", "")+".py"
    purl = "/".join([GITLAB_PUBLIC_IP, project.namespace['path'], project.path])
    url = ""
    if which[0] > which[1]:
        url = "/".join([purl, ppath, pfile])
    else:
        url = "/".join([purl, jpath, jfile])
    form = Review()
    if request.method == 'GET':
        return render_template('review_template.html', form=form, url=url)
    if request.method == "POST" and not form.validate_on_submit():
        return render_template('review_template.html', form=form, url=url)
    review_path = review_file(project.path, giving_user, receiving_user)
    review_text = "@"+receiving_user+" - you have a new review\n\n"
    with open(review_path, "w") as reviewfile:
        reviewfile.write("submit_time=")
        reviewfile.write(time.asctime())
        reviewfile.write('\n')
        for data in form:
            if data.id in ['submit', 'csrf_token']:
                # don't store framework variables
                continue
            reviewfile.write(data.id)
            review_text += str(data.label)
            reviewfile.write("=")
            review_text += " = "
            reviewfile.write(str(data.data))
            review_text += str(data.data)
            reviewfile.write('\n')
            review_text += '\n\n'
    new_issue = project.issues.create({
        'title': 'Review',
        'description': review_text
    })
    report_to_mainserver('REVIEW_EVAL', receiving_user, username_hash(giving_user), project.path, new_issue.iid, form.review_secure.data, form.review_correct.data, form.review_good.data)
    return render_template('review_recorded.html')


@app.route('/questions/<questname>/<taskname>', methods=['GET', 'POST'])
def task_questionnaire(questname, taskname):
    try:
        user = validate_user()
    except Exception as e:
        return str(e), 401
    if not metric_reporting_on():
        consent_msg = "admin"
    elif not user_did_consent(user.username):
        consent_msg = "user"
    else:
        consent_msg = None
    if questname not in ['demo', 'task', 'final']:
        return "Incorrect questionnaire name "+questname, 404
    if questname == 'task' and taskname not in list_of_tasks:
        return "Incorrect task name "+taskname, 404
    if questname == 'task': # we won't send this back but we need to show the form for it anyway, so no consent needed
        consent_msg = None
    if request.method == 'GET':
        if questname == 'demo':
            return render_template('form_template.html', form=DemographicQuestionnaire(), consent_msg=consent_msg, page_title="Demographics Questionnaire")
        elif questname == 'task':
            return render_template('form_template.html', form=TaskQuestionnaire(), consent_msg=consent_msg, page_title=taskname.replace("-"," ").capitalize()+" Questionnaire")
        elif questname == 'final':
            return render_template('form_template.html', form=FinalQuestionnaire(), consent_msg=consent_msg, page_title="Final Questionnaire")
    else:
        form = None
        pagetitle = "Questionnaire"
        if questname == 'demo':
            form = DemographicQuestionnaire()
            pagetitle = "Demographics Questionnaire"
        elif questname == 'task':
            pagetitle = taskname.replace("-"," ").capitalize()+" Questionnaire"
            form = TaskQuestionnaire()
        elif questname == 'final':
            pagetitle = "Final Questionnaire"
            form = FinalQuestionnaire()
            if not user_did_consent(user):
               return render_template('form_template.html', form=form, consent_msg=consent_msg, page_title=pagetitle)
        if form.validate_on_submit():
            header_line = ""
            data_line=""
            for data in form:
                if data.id in ['submit', 'csrf_token']:
                    # don't store framework variables
                    continue
                header_line = header_line + "," + data.id
                data_line = data_line + "," + str(data.data).replace(",", ";")
            report_to_mainserver(questname, user, taskname, header_line, data_line)
            return render_template('form_template.html', consent_msg="done", page_title=pagetitle)
        else:
            return render_template('form_template.html', form=form, consent_msg=consent_msg, page_title=pagetitle)

# metric recording

app_metrics = ['gitlab', 'server', 'game']
game_metrics = [
    'GL_START',
    'GL_ENDED',
    'GL_QUIT',
    'G_UPG_UN',
    'G_UPG_IMP',
    'GL_TOWER',
    'GL_ENEMY_WIN',
    'GL_ENEMY_LOSS',
    'GL_TEXT',
    'GL_WAVE_START'
]
gitlab_metrics = [
    'TASK_START',
    'REVIEW_READ',
    'SOL_FILE'
]

def clean_metric_common_data(request_data_name):
    if request_data_name not in request.form:
        raise Exception("Data missing")
    if request_data_name == "level_name" and request.form['level_name'] in ["menu", "tutorial", "game", "Level1", "Level2.1", "MainScene"]:
        return request.form['level_name']
    if request_data_name == "ts":
        return request.form['ts'] # Unix timestamp form
    if request_data_name == "level_ts":
        return int(request.form['level_ts'])
    if request_data_name == "task_name" and (request.form['task_name'] in list_of_tasks or request.form['task_name'] == "-"):
        return request.form['task_name']
    if request_data_name == "username":
        return request.form['username']
    raise Exception("Invalid data for metric with data " + request_data_name)

def clean_metric_specific_data(metricname):
    metrics_with_no_additional_data = ['TASK_START', 'GL_START', 'GL_QUIT']
    int_metrics = ['GL_WAVE_START']
    bool_metrics = []
    str_metrics = ['GL_TOWER', 'G_UPG_UN', 'GL_ENEMY_WIN', 'GL_ENEMY_LOSS', 'GL_ENDED', 'G_UPG_IMP', 'GL_TOWER_HACK', 'UNLOCKCODE', 'GL_TEXT']
    if metricname in metrics_with_no_additional_data:
        return ['']
    data = request.form['data']
    if metricname in str_metrics:
        return [data.split(",")]
    if metricname in int_metrics:
        return [int(data)]
    if metricname in bool_metrics and data in ['true', 'false']:
        return [data]
    if metricname == "REVIEW_READ":
        return [data.split("/")[2], data.split("/")[5]]
    if metricname == "SOL_FILE":
        return [username_hash(data.split("/")[1]), data.split("/")[2], "/".join(data.split("/")[4:])]
    raise Exception("Invalid data for metric " + metricname)

@app.route("/metric", methods=["POST"])
def metric():
    appname = request.form['app']
    metricname = request.form['metric']
    # validate the data types
    if appname not in app_metrics:
        return "bad app", 404
    if appname == "game" and metricname not in game_metrics:
        return "bad metric", 404
    if appname == "gitlab" and metricname not in gitlab_metrics:
        return "bad metric", 404
    # the game has a token, but the main webpage does not. ugh.
    if appname == "game":
        try:
            user = validate_user()
        except Exception as e:
            return str(e), 401
        username = user.username
    if appname == "gitlab":
        username = clean_metric_common_data('username')
    # add the mandatory data columns
    columns = [username, clean_metric_common_data('ts'), *clean_metric_specific_data(metricname)]
    if appname == "game":
        columns.append(clean_metric_common_data('level_name'))
        columns.append(clean_metric_common_data('level_ts'))
    if appname == "gitlab":
        columns.append(clean_metric_common_data('task_name'))
    # write the data
    path = os.path.join(DATA_PATH, "metrics", appname, metricname)
    os.makedirs(os.path.dirname(path), exist_ok=True)
    with open(path, "a") as metricfile:
        for col in columns:
            metricfile.write(str(col).replace(',',';'))
            metricfile.write(',')
        metricfile.write("\n")
    report_to_mainserver(metricname, username, *columns[2:])
    return "", 204

def manual_metric(metricname, user, *data):
    appname = "server"
    username = user if isinstance(user, str) else user.username
    ts = time.time()
    columns = [username, str(ts), *data]
    path = os.path.join(DATA_PATH, "metrics", appname, metricname)
    os.makedirs(os.path.dirname(path), exist_ok=True)
    with open(path, "a") as metricfile:
        for col in columns:
            metricfile.write(str(col).replace(',',';'))
            metricfile.write(',')
        metricfile.write("\n")
    report_to_mainserver(metricname, username, *data)

class ConsentForm(FlaskForm):
    consent = RadioField(label="Have you read all the required documents and consent to take part?", choices=(('True', 'Yes, take part'), ('False', 'No, continue without taking part')))
    submit = SubmitField()

class AdminConsentForm(FlaskForm):
    consent = RadioField(label="Have you read all the required documents, meet the requirements, and consent to take part?", choices=(('True', 'Yes, take part'), ('False', 'No, set up server without taking part')))
    submit = SubmitField()

@app.route("/consent", methods=["GET", "POST"])
def consent():
    try:
        user = validate_user()
    except Exception as e:
        return str(e), 401
    form = ConsentForm()
    if request.method == "GET":
        if user_did_consent(user):
            form.consent.choice = 'True'
        return render_template("consent.html", form=form)
    else:
        if form.validate_on_submit():
            path = os.path.join(DATA_PATH, "consent", user.username)
            os.makedirs(os.path.dirname(path), exist_ok=True)
            with open(path, "w")as consentfile:
                consentfile.write(str(form.consent.data))
            report_to_mainserver("CONSENT", user, form.consent.data, consent_override=True)
            if 'popup' in request.args:
                return "Consent updated. You may now close this tab"
            return redirect(GITLAB_PUBLIC_IP+"/assets/custom/game/index.html?token=" + request.args['token'])
        else:
            return render_template('form_template.html', form=form)

def user_did_consent(username):
    if not isinstance(username, str):
        username = username.username
    path = os.path.join(DATA_PATH, "consent", username)
    if os.path.exists(path):
        with open(path, "r") as consentfile:
            if consentfile.read().strip() == "True":
                return True
    return False

def username_hash(username):
    if not isinstance(username, str):
        username = username.username
    return hashlib.sha256(username.encode("utf8")).hexdigest()

def report_to_mainserver(metric_name, user, col1, col2="", col3="", col4="", col5="", col6="", col7="", col8="", col9="", col10="", consent_override=False):
    if not consent_override and not metric_reporting_on():
        return
    if not isinstance(user, str):
        user = user.username
    if not consent_override and not user_did_consent(user):
        # use consent_override to report consent. we need to override it,
        # as we need to record it, even if it is "false", in case a
        # user wants to change their mind at a later date
        # the override is also allowed in the case of tracking modifications to CI files
        return
    azure = "https://prod-167.westeurope.logic.azure.com:443/workflows/142d930638614b8891353dd12077dff0/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=6JGN971RTGCPhK_zC2OeAwT72mE9D4DQssI732KtYk0"
    with open(os.path.join(DATA_PATH, "install_guid"), "r") as uuidfile:
        UUID = uuidfile.read().strip()
    data_to_post = {
        "uuid": UUID,
        "user": username_hash(user),
        "timestamp": str(time.time()),
        "metric_name": metric_name,
        "col1": str(col1),
        "col2": str(col2),
        "col3": str(col3),
        "col4": str(col4),
        "col5": str(col5),
        "col6": str(col6),
        "col7": str(col7),
        "col8": str(col8),
        "col9": str(col9),
        "col10": str(col10)
    }
    with open(os.path.join(DATA_PATH, "servermirror"), "a") as logfile:
        logfile.write(str(data_to_post)+"\n")
    r = post(azure, json=data_to_post)
