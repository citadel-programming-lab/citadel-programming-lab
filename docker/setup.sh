#!/bin/bash

# Citadel Programming Lab
# Copyright (C) 2017-present  Heriot-Watt University, Glasgow School of Art
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

if [ $# -ne 3 ]
then
    echo "Usage: $0 directory url admin_password"
    exit 1
fi

# set the environment variables
export LAB_HOME="$1"
export URL="$2"
export PASSWD="$3"
export CERTFILE="$4"
export CERTKEY="$5"
export CERTCHAIN="$6"
export TOKEN="$RANDOM$RANDOM"

echo "Chose $TOKEN as random token"


echo "Building gameserver docker image"
cd ..
docker build -t gameserver -f docker/Dockerfile .
cd docker

# start docker
echo "Starting Docker Images"
docker-compose up -d >/dev/null 1>/dev/null 2>/dev/null &

while [ "$(docker inspect --format '{{ json .State.Status }}' gitlab)" != "\"running\"" ]
do
    echo "Waiting for GitLab to be running"
    sleep 5
done
echo "Adding files to GitLab"
sleep 3
echo "Copying certificates"
docker exec gitlab mkdir -p /etc/gitlab/ssl
docker cp $CERTFILE gitlab:/$URL.crt
docker exec gitlab mv $URL.crt /etc/gitlab/ssl/$URL.crt
docker cp $CERTKEY gitlab:/$URL.key
docker exec gitlab mv $URL.key /etc/gitlab/ssl/$URL.key
echo "Tarballing assets"
rm gitlab-config.tar
tar -cf gitlab-config.tar -C .. opt/gitlab/embedded/service/gitlab-rails/public/assets/custom
echo "Copying assets"
docker exec gitlab mkdir -p /opt/gitlab/embedded/service/gitlab-rails/public/assets/custom
docker cp gitlab-config.tar gitlab:/
echo "Extracting assets"
docker exec gitlab tar xf gitlab-config.tar

while [ "$(docker inspect --format '{{ json .State.Health.Status }}' gitlab)" != "\"healthy\"" ]
do
    echo "Waiting for gitlab to be healthy"
    docker exec gitlab chmod 777 /var/opt/gitlab/gitlab-workhorse/sockets/socket
    sleep 20
done

while [ "$(docker inspect --format '{{ json .State.Status }}' gitlab-runner)" != "\"running\"" ]
do
    echo "Attempting to start runner"
    docker-compose up -d runner >/dev/null 1>/dev/null 2>/dev/null &
    docker exec gitlab chmod 777 /var/opt/gitlab/gitlab-workhorse/sockets/socket
    sleep 20
done
echo "adding certificates to runner"
docker cp $CERTCHAIN gitlab-runner:/$URL.crt
docker exec gitlab-runner mkdir -p /etc/gitlab-runner/certs/
docker exec gitlab-runner mv /$URL.crt /etc/gitlab-runner/certs/$URL.crt
echo "registering runner"
docker exec gitlab chmod 777 /var/opt/gitlab/gitlab-workhorse/sockets/socket
docker exec gitlab-runner gitlab-runner register --non-interactive --executor "docker" --docker-image gitlab/gitlab-runner:latest --url https://$URL --registration-token "$TOKEN" --description "docker-runner" --tag-list "docker" --run-untagged="true" --locked="false" --access-level="not_protected"

docker exec -ti gameserver sh -c "echo $URL > /usr/data/host"
docker exec gitlab sed -i "s/HOSTNAME/$URL/g" /opt/gitlab/embedded/service/gitlab-rails/public/assets/custom/custom-lab.js

echo "Please visit this URL and log in:"
echo "https://$URL"
read -p "Press enter to continue"

echo "Please visit this URL"
echo "https://$URL/admin/application_settings/network#js-outbound-settings"
echo "Add 172.18.0.4:5000 to allowed local IP addresses and click save"
read -p "Press enter to continue"

echo "Please visit this URL"
echo "https://$URL/admin/applications/new"
echo "And make a new application."
echo "   Name GameAuth"
echo "   Redirect URI https://$URL:5000/oauth"
echo "   Trusted ✓"
echo "   Confidential ×"
echo "   Scopes read_user, read_api"
echo "   Click submit"
read -p "Press enter to continue"

read -p "Enter the application ID" appid
read -p "Enter the secret" secret
docker exec gitlab sed -i "s/OAUTHAPP/$appid/g" /opt/gitlab/embedded/service/gitlab-rails/public/assets/custom/custom-lab.js
docker exec -ti gameserver sh -c "echo $appid > /usr/data/oauth_appid"
docker exec -ti gameserver sh -c "echo $secret > /usr/data/oauth_secret"

echo "Visit https://$URL/admin/application_settings/general#js-account-settings"
echo "Disable: Gravatar, user oauth apps, prompt ssh keys"
echo "Set the default roject limit 6"
echo "Set the maximum attachment size to 1"
echo "Set the maximum push size to 5"
read -p "Press enter to continue"

echo "Visit https://$URL/admin/application_settings/general#js-signup-settings"
echo "Disable: sign-up enabled"
read -p "Press enter to continue"

echo "Please visit this URL"
echo "https://$URL:5000"
echo "Follow the link to make a sudo+api access token"
echo "And paste it back into the original form, and submit"
echo "The initial setup process will take some time,"
echo "depending on how many users are added"
read -p "Press enter to continue"

echo "If you want to participate in the experiment,"
echo "and only if you agree to the consent form, visit"
echo "https://$URL:5000/teacher/metrics/toggle"
echo "This will share some data back to us for analysis"
read -p "Press enter to continue"


echo "System Ready and Running."
echo "  Deactivate with docker-compose stop"
echo "  Next time you want to start, use docker-compose start "
