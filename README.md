# Citadel Programming Lab, a Secure Coding Game-Based Lab
The secure coding game based lab takes two components - a game and
coding exercises and mixes them together. It is designed for use in a
lab environment with multiple students. It teaches security concepts aligned with [CyBOK](https://www.cybok.org/). Visit the project page to find out more: [Citadel Programming Lab](https://citadel-programming-lab.gitlab.io/).

https://citadel-programming-lab.gitlab.io/

This document is the readme for the docker configuration for running
and packaging the lab.

## Contents of the Lab
*Game Server* - Included This is the part of the lab that will run on the server. It runs as a composition of docker machines. It includes a [GitLab](https://www.gitlab.com/) instance, a GitLab runner, and an orchestration server. in this repository.
*Tower Defence Game* -  This is a video game built in [Unity](https://unity.com/), targeting WebGL. A pre-built copy is included with the server configuration. The source code is available in a separate repository.

## Requirements
* This requires a Linux OS. It will not run under Windows / WSL
* At least the minimum requirements for docker, listed here:
    * https://docs.docker.com/engine/install/
* At least the minimum hardware requirements for GitLab, listed here:
    * https://docs.gitlab.com/ee/install/requirements.html
* Total Hardware requirements:
    * 64-bit 4-core Processor with Virtualisation support
    * 8GB RAM
    * At least 12 GB Storage, increase by 0.01 gb for every additional
      user. Use the command `docker ps --size` and/or `df -h` to monitor disk usage.
    * Internet Access (for initial setup of docker containers, and optional reporting of metrics)
* Software Requirements:
    * docker (container builder, engine & composer)

* To access the lab teacher and students will need a modern web browser, either Firefox or Chrome based.

### Tested with the following system
* CentOS 7.9, 64-bit, 8 CPU Cores, 20GB Ram, 50GB Storage

# Usage & Administration
This section describes how to set up and administer the lab.

## Initial Setup
1. Make sure the Linux server which will run the lab has the docker software installed and meets the requirements
2. Clone this repository onto the server
3. Generate an HTTPS certificate for the server. It does not support letsencrypt auto generation, so a certificate must be obtained manually.
4. Visit the file at `opt/gitlab/embedded/service/gitlab-rails/public/assets/custom/game/index.html` and edit the URL at the end of line 6 to include the domain that will serve the lab
5. Visit the file at `gameserver/serverapp/app.py` and change the `app.secret_key` to something new and random
5. Enter the docker directory within this repository: `cd docker`
6. Copy your HTTPS certificate, key and chain into this docker build directory
    - The certficiate chain must be pem-encoded. you can obtain it by visiting the site in firefox, going to certificate view and clicking the export chain button
7. Perform a first time setup. Run this command, choosing appropriate variables:
    - `./setup.sh STORAGE_DIR ROOT_PASSWORD GITLAB_HOSTNAME PATH_TO_CERT PATH_TO_CERT_KEY PATH_TO_CERT_CHAIN`
8. Run through the setup script. Initially setting up gitlab will take some time.
9. Some parts will require you to visit a web page, follow the instructions indicated at the command line

Once initially set up, if you need to stop it, enter the docker directory and use the command `docker-compose stop` and `docker-compose start` as needed.
If you are using a different session you will need to make sure the relevant environment variables are set.
These are the same ones as with step 7, e.g.
```
export LAB_HOME=path_to_storage_dir
export URL=gitlab_hostname
```

## Internet access
The following information is relevant for setting up firewalls:

GitLab can be accessed via HTTPS on port 443 of the machine. The teacher interface is accessible at port 5000.
Students don't access the teacher interface, but do need access to port 5000 for an oauth redirection step.
The lab is not configured for SSH / local git cloning access.

The GitLab runner does not need to have external access.

## Webserver setup
This takes place on the URL:5000 webpage indicated near the end of running
the setup script. This expects a token, created with sudo and api access rights, and a list of users.

The users should be prepared in CSV format: `username,email,password`

The steps that will be run are:
* Adding users
* Import pre-complete tasks 
* Write access tokens
* Fork projects
* Add project hooks

Additional users can be added at a later date from the teacher interface, and this form accepts a username, email address and password.

Users need to be sent their login details directly as the lab is not configured to send out emails.

# Teacher Instructions
Once it is set up, Visit the gameserver page at
https://URL:5000/teacher to get an overview of how students are
progressing with the tasks. Tasks are indicated by:
* White: Unstarted
* Red: Started, not passing tests
* Green: Complete, passing tests
The main page offers a summary of how many reviews have been given/received. It also shows the students top score in the game.

You can also click a user to get more detailed information on how they
are performing in each of the tasks. This view includes languages used, commits made per task, and the state of continuous integration. This view also lets you see the students reviews, if you wish to moderate them.

## Experiment Participation
We are running an ongoing experiment gathering data from use of this lab.
The teacher page on the gameserver contains a link to join in to
sharing data for the experiment, as well as links to a privacy policy
and detailed information about the experiment. If you feel comfortable
doing so, we would appreciate data be sent back to us.

If you need to contact us about the experiment, be sure to include the
debug info at the bottom of the main teacher page, if you can see it.

Metrics can be enabled or disabled by visiting URL: http://URL:5000/teacher/metrics/toggle

## The Game
The game is a tower defence game. Within the game, there are upgrades.
These are unlocked by completing tasks. It is possible to play the game without completing any of the tasks, however students are incentivised to complete tasks as otherwise upgrades will be locked and unusable.

Within the game there is a tutorial level, an in-game help system, and a summary of CyBOK concepts relevant to the game and tasks.

The game also has a leaderboard. This leaderboard displays the students display name. If they wish to change it, they should do so from within GitLab at the following URL: https://URL/-/profile under "Full Name".

## The tasks
There are 6 tasks in this lab. Each task has a security concern for students to consider:

* URL Task - Asking students to shorten a URL
* Encryption task - Asking students to encrpyt some text
* Credentials Task - Asking students to securely store a password
* PGP Task - Implement signing and verification services with PGP
* SSL Task - Complete an SSL handshake
* X509 Task - This is currently not implemented

The lab aims to teach concepts from the CyBOK (Cyber Security Body Of Knowledge) project. More information on CyBOK can be found here: [cybok.org](https://www.cybok.org).

# FAQs & Debugging

*Peer reviews show a different language to the one the reviewer used*
Peer review does not consider the language that a student made their
own solution in. So if 2 students each complete either a solution in
java or python, when they need to review it will use the other language.

*The server reports a student has made more commits than they actually made*
The teacher view lists the commit count including the initial commits in the template repository,
so bear this in mind when calculating student engagement. The repositories have the following initial commits:
* URL Task - 2
* Encryption task - 2
* Credentials Task - 2
* PGP Task - 5
* SSL Task - 20
* X509 Task - 4

*Gitlab keeps giving a 502 error*
During initial setup, it may do this, and the setup script may show this error: 
```
chmod: cannot access '/var/opt/gitlab/gitlab-workhorse/sockets/socket': No such file or directory
```
This is fine, wait about 10 minutes and eventually docker will finish configuration and move on to the next step.

After initial setup, if gitlab does not start, or only gives you 502 errors, check the logs
like so: `docker logs --follow gitlab`. If you see an error
referencing `/var/opt/gitlab/gitlab-workhorse/sockets/socket`, then
re-run this command: `docker exec gitlab chmod 777 /var/opt/gitlab/gitlab-workhorse/sockets/socket`

*There are no gitlab runners available for the continuous integration testing*
Visit http://URL/admin/runners logged in as admin, and click the 👁
and then note the token.
Then run the following command, adding in the token as needed.
```
docker exec gitlab-runner gitlab-runner register --non-interactive --executor "docker" --docker-image gitlab/gitlab-runner:latest --url http://172.18.0.2 --registration-token "TOKEN" --description "docker-runner" --tag-list "docker" --run-untagged="true" --locked="false" --access-level="not_protected"
```


*There are some users listed in the teacher interface that I don't recognise*
There are two additional users, aa001 and bb002 which contain sample code that other students can use for reviews if they are the first students finished. These can be ignored.
Gitlab may also add bots, which have tasks "blacked out" in the teacher interface. These do not participate in the lab and can be ignored.

*The teacher interface is very slow*
This is a known limitation of the platform. Consider accessing the teacher interface during less busy periods.

*I get timeout errors when using the teacher interface*
Sometimes the webserver setup or user creation fails with a "timeout" error. If this happens, just reload the page (if it asks to resubmit the form, say yes or continue). The setup is able to recover from such situations if re-run.

*Students get a WebGL error when playing the game, or the game doesn't start*
You need to use a web browser that offers up to date WebGL support, and which takes use of graphics acceleration. Web browsers accessed over ssh, VNC, x2go etc may not support this. Safari and older web browsers are not supported. Please use an up to date build of firefox or a chrome based web browser.
