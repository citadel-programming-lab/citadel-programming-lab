/**
* Custom JS extension for GitLab

Citadel Programming Lab
Copyright (C) 2017-present  Heriot-Watt University, Glasgow School of Art

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
**/
"use strict";

/**
 * Add all of the individual scripts,
 * and then call their init methods
 */
function addResources() {
    if(document && document.readyState === "complete"){
        addCSS();
        addHelp();
        addLinkToGame();
        addWebIdeMods();
        reportSpecialPageReads();
    } else {
        setTimeout(addResources, 100);
    }
}

/**
 * Modify the page only on regular pages, not asset or admin ones
 */
const BLOCKED_PAGES = [
    /assets/,
    /profile/,
    /sign_in/,
    /admin/
];
if(!BLOCKED_PAGES.find(x => window.location.href.match(x))){
    setTimeout(addResources, 100);
}

const tasknames = ["credentials-task", "encryption-task", "url-task","ssl-task","pgp-task","x509-task"];

/**
 * Analytic
 */
async function analytic(metric, data) {
    const formData = new FormData();
    formData.append('data', data);
    let taskname = '-'
    for (let i = 0; i < tasknames.length; i++) {
        if(window.location.pathname.indexOf(tasknames[i]) >= 0){
            taskname = tasknames[i];
            break;
        }
    }
    formData.append('task_name', taskname);
    formData.append('ts', Date.now());
    formData.append('app', 'gitlab');
    formData.append('metric', metric);
    formData.append('username', gon.current_username);
    fetch(window.origin+":5000/metric", {
        method: 'POST',
        body: formData,
        mode: 'no-cors'
    })
}


/**
 * Add a link to the game to the page header
 * @remark this should run on all pages
 */
async function addLinkToGame(){
    const nav = document.querySelector("ul.nav");
    const rand = btoa(gon.current_username + new Date().toISOString())
    const testgameurl = `/oauth/authorize?client_id=OAUTHAPP&redirect_uri=${window.origin}:5000/oauth&response_type=code&state=${rand}&scope=read_user+read_api&code_challenge=6MMWL7vpXUcOes0R0_fKQyPY3Fty8epd2M7qGisF1Bo&code_challenge_method=S256`;
    nav.insertAdjacentHTML("afterbegin", `<li id="gameLink" class="d-none d-xl-block" style="display: block !important;">
    <a class="" target="_blank" title="Launch the game" href="${testgameurl}">Launch Game</a>
    </li>`);

    // also add special link after first login
    if(window.location.pathname==="/"){
        const pageContentHeader = document.querySelector("h1.page-title");
        pageContentHeader.insertAdjacentHTML("afterbegin", `<a href="${testgameurl}" class="btn btn-confirm btn-md gl-button">
        <span class="gl-button-text">Launch Game</span></a>`)
    }
}


/**
 * Add help link
 */
function addHelp(){
    const nav = document.querySelector("ul.nav");
    const helpUrl = window.origin+`:5000/static/instructions.htm`;
    nav.insertAdjacentHTML("afterbegin", `<li id="gameLink" class="d-none d-xl-block" style="display: block !important;">
    <a class="" target="_blank" title="View Instructions" href="${helpUrl}">View Instructions</a>
    </li>`);
    // also add special link after first login
    if(window.location.pathname==="/"){
        const pageContentHeader = document.querySelector("h1.page-title");
        pageContentHeader.insertAdjacentHTML("afterbegin", `<a href="${helpUrl}" class="btn btn-confirm btn-md gl-button">
        <span class="gl-button-text">Open Help</span></a>`)
    }
}

function addCSS() {
    document.head.insertAdjacentHTML("beforeend", "<link rel='stylesheet' href='/assets/custom/custom-lab.css' />");
}

/* send some analytics when certain pages are opened */
function reportSpecialPageReads(){
    if(window.location.pathname.indexOf("/issues/") >= 0){
        analytic("REVIEW_READ", window.location.pathname);
    }
    if(window.location.pathname.match(/\/(blob|commit)\//)){
        analytic("SOL_FILE", window.location.pathname);
    }
}

/* entry point for modifications to web ide
currently only change is to force use of master branch */
function addWebIdeMods(){
    if(window.location.pathname.indexOf("-/ide") >= 0){
        analytic("TASK_START");
        setInterval(() => {
            if(document.querySelector("input[name='commit-action'][value='1']")){
                document.querySelector("input[name='commit-action'][value='1']").click();
            }
        }, 500);
    }
}
